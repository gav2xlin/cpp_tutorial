#include <iostream>

using namespace std;

class Box {
public:
    double length;         // Length of a box
    double breadth;        // Breadth of a box
    double height;         // Height of a box

    // Member functions declaration
    double getVolume(void);
    void setLength( double len );
    void setBreadth( double bre );
    void setHeight( double hei );

    void setWidth( double wid );
    double getWidth( void );
private:
    double width;
};

// Member functions definitions
double Box::getWidth(void) {
    return width ;
}

void Box::setWidth( double wid ) {
    width = wid;
}

// Member functions definitions
double Box::getVolume(void) {
    return length * breadth * height;
}

void Box::setLength( double len ) {
    length = len;
}
void Box::setBreadth( double bre ) {
    breadth = bre;
}
void Box::setHeight( double hei ) {
    height = hei;
}

class Line {
public:
    double length;
    void setLength( double len );
    double getLength( void );
};

// Member functions definitions
double Line::getLength(void) {
    return length ;
}

void Line::setLength( double len) {
    length = len;
}

class _Box {
protected:
    double width;
};

class SmallBox:_Box { // SmallBox is the derived class.
public:
    void setSmallWidth( double wid );
    double getSmallWidth( void );
};

// Member functions of child class
double SmallBox::getSmallWidth(void) {
    return width ;
}

void SmallBox::setSmallWidth( double wid ) {
    width = wid;
}

class Line2 {
public:
    void setLength( double len );
    double getLength( void );
    Line2();  // This is the constructor
    Line2(double len);  // This is the constructor
    ~Line2();  // This is the destructor: declaration
private:
    double length;
};

// Member functions definitions including constructor
Line2::Line2(void) {
    cout << "Object is being created" << endl;
}

// Member functions definitions including constructor
/*Line2::Line2( double len) {
    cout << "Object is being created, length = " << len << endl;
    length = len;
}*/

Line2::Line2( double len): length(len) {
    cout << "Object is being created, length = " << len << endl;
}

Line2::~Line2(void) {
    cout << "Object is being deleted" << endl;
}

void Line2::setLength( double len ) {
    length = len;
}

double Line2::getLength( void ) {
    return length;
}

class Line3 {
public:
    int getLength( void );
    Line3( int len );             // simple constructor
    Line3( const Line3 &obj);  // copy constructor
    ~Line3();                     // destructor

private:
    int *ptr;
};

// Member functions definitions including constructor
Line3::Line3(int len) {
    cout << "Normal constructor allocating ptr" << endl;

    // allocate memory for the pointer;
    ptr = new int;
    *ptr = len;
}

Line3::Line3(const Line3 &obj) {
    cout << "Copy constructor allocating ptr." << endl;
    ptr = new int;
    *ptr = *obj.ptr; // copy the value
}

Line3::~Line3(void) {
    cout << "Freeing memory!" << endl;
    delete ptr;
}

int Line3::getLength( void ) {
    return *ptr;
}

void display(Line3 obj) {
    cout << "Length of line : " << obj.getLength() <<endl;
}

class Box2 {
    double width;

public:
    friend void printWidth( Box2 box );
    void setWidth( double wid );
};

// Member function definition
void Box2::setWidth( double wid ) {
    width = wid;
}

// Note: printWidth() is not a member function of any class.
void printWidth( Box2 box ) {
    /* Because printWidth() is a friend of Box, it can
    directly access any member of this class */
    cout << "Width of box : " << box.width <<endl;
}

inline int Max(int x, int y) {
    return (x > y)? x : y;
}

class Box3 {
public:
    // Constructor definition
    Box3(double l = 2.0, double b = 2.0, double h = 2.0) {
        cout <<"Constructor called." << endl;
        length = l;
        breadth = b;
        height = h;
    }
    double Volume() {
        return length * breadth * height;
    }
    int compare(Box3 box) {
        return this->Volume() > box.Volume();
    }

private:
    double length;     // Length of a box
    double breadth;    // Breadth of a box
    double height;     // Height of a box
};

class Box4 {
public:
    static int objectCount;

    // Constructor definition
    Box4(double l = 2.0, double b = 2.0, double h = 2.0) {
        cout <<"Constructor called." << endl;
        length = l;
        breadth = b;
        height = h;

        // Increase every time object is created
        objectCount++;
    }
    double Volume() {
        return length * breadth * height;
    }
    static int getCount() {
        return objectCount;
    }

private:
    double length;     // Length of a box
    double breadth;    // Breadth of a box
    double height;     // Height of a box
};

// Initialize static member of class Box
int Box4::objectCount = 0;

int main() {
    {
        Box Box1;        // Declare Box1 of type Box
        Box Box2;        // Declare Box2 of type Box
        double volume = 0.0;     // Store the volume of a box here

        // box 1 specification
        Box1.height = 5.0;
        Box1.length = 6.0;
        Box1.breadth = 7.0;

        // box 2 specification
        Box2.height = 10.0;
        Box2.length = 12.0;
        Box2.breadth = 13.0;

        // volume of box 1
        volume = Box1.height * Box1.length * Box1.breadth;
        cout << "Volume of Box1 : " << volume << endl;

        // volume of box 2
        volume = Box2.height * Box2.length * Box2.breadth;
        cout << "Volume of Box2 : " << volume << endl;
    }

    {
        Box Box1;                // Declare Box1 of type Box
        Box Box2;                // Declare Box2 of type Box
        double volume = 0.0;     // Store the volume of a box here

        // box 1 specification
        Box1.setLength(6.0);
        Box1.setBreadth(7.0);
        Box1.setHeight(5.0);

        // box 2 specification
        Box2.setLength(12.0);
        Box2.setBreadth(13.0);
        Box2.setHeight(10.0);

        // volume of box 1
        volume = Box1.getVolume();
        cout << "Volume of Box1 : " << volume <<endl;

        // volume of box 2
        volume = Box2.getVolume();
        cout << "Volume of Box2 : " << volume <<endl;
    }

    {
        Line line;

        // set line length
        line.setLength(6.0);
        cout << "Length of line : " << line.getLength() <<endl;

        // set line length without member function
        line.length = 10.0; // OK: because length is public
        cout << "Length of line : " << line.length <<endl;
    }

    {
        Box box;

        // set box length without member function
        box.length = 10.0; // OK: because length is public
        cout << "Length of box : " << box.length <<endl;

        // set box width without member function
        // box.width = 10.0; // Error: because width is private
        box.setWidth(10.0);  // Use member function to set it.
        cout << "Width of box : " << box.getWidth() <<endl;
    }

    {
        SmallBox box;

        // set box width using member function
        box.setSmallWidth(5.0);
        cout << "Width of box : "<< box.getSmallWidth() << endl;
    }

    {
        Line2 line;

        // set line length
        line.setLength(6.0);
        cout << "Length of line : " << line.getLength() <<endl;
    }

    {
        Line2 line(10.0);

        // get initially set length.
        cout << "Length of line : " << line.getLength() <<endl;

        // set line length again
        line.setLength(6.0);
        cout << "Length of line : " << line.getLength() <<endl;
    }

    {
        Line2 line;

        // set line length
        line.setLength(6.0);
        cout << "Length of line : " << line.getLength() <<endl;
    }

    {
        Line3 line(10);

        display(line);
    }

    {
        Line3 line1(10);

        Line3 line2 = line1; // This also calls copy constructor

        display(line1);
        display(line2);
    }

    {
        Box2 box;

        // set box width without member function
        box.setWidth(10.0);

        // Use friend function to print the wdith.
        printWidth( box );
    }

    {
        cout << "Max (20,10): " << Max(20,10) << endl;
        cout << "Max (0,200): " << Max(0,200) << endl;
        cout << "Max (100,1010): " << Max(100,1010) << endl;
    }

    {
        Box3 box1(3.3, 1.2, 1.5);    // Declare box1
        Box3 box2(8.5, 6.0, 2.0);    // Declare box2

        if(box1.compare(box2)) {
            cout << "Box2 is smaller than Box1" <<endl;
        } else {
            cout << "Box2 is equal to or larger than Box1" <<endl;
        }
    }

    {
        Box3 Box1(3.3, 1.2, 1.5);    // Declare box1
        Box3 Box2(8.5, 6.0, 2.0);    // Declare box2
        Box3 *ptrBox;                // Declare pointer to a class.

        // Save the address of first object
        ptrBox = &Box1;

        // Now try to access a member using member access operator
        cout << "Volume of Box1: " << ptrBox->Volume() << endl;

        // Save the address of second object
        ptrBox = &Box2;

        // Now try to access a member using member access operator
        cout << "Volume of Box2: " << ptrBox->Volume() << endl;
    }

    {
        Box4 Box1(3.3, 1.2, 1.5);    // Declare box1
        Box4 Box2(8.5, 6.0, 2.0);    // Declare box2

        // Print total number of objects.
        cout << "Total objects: " << Box4::objectCount << endl;
    }

    {
        // Print total number of objects before creating object.
        cout << "Inital Stage Count: " << Box4::getCount() << endl;

        Box4 Box1(3.3, 1.2, 1.5);    // Declare box1
        Box4 Box2(8.5, 6.0, 2.0);    // Declare box2

        // Print total number of objects after creating object.
        cout << "Final Stage Count: " << Box4::getCount() << endl;
    }

    return 0;
}