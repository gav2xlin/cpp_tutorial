#include <iostream>

using namespace std;

int main() {
    {
        // Local variable declaration:
        int a = 10;

        // while loop execution
        while( a < 20 ) {
            cout << "value of a: " << a << endl;
            a++;
        }
    }

    {
        // for loop execution
        for( int a = 10; a < 20; a = a + 1 ) {
            cout << "value of a: " << a << endl;
        }
    }

    {
        // Local variable declaration:
        int a = 10;

        // do loop execution
        do {
            cout << "value of a: " << a << endl;
            a = a + 1;
        } while( a < 20 );
    }

    {
        int i, j;

        for(i = 2; i<100; i++) {
            for(j = 2; j <= (i/j); j++)
                if(!(i%j)) break; // if factor found, not prime
            if(j > (i/j)) cout << i << " is prime\n";
        }
    }

    {
        // Local variable declaration:
        int a = 10;

        // do loop execution
        do {
            cout << "value of a: " << a << endl;
            a = a + 1;
            if( a > 15) {
                // terminate the loop
                break;
            }
        } while( a < 20 );
    }

    {
        // Local variable declaration:
        int a = 10;

        // do loop execution
        do {
            if( a == 15) {
                // skip the iteration.
                a = a + 1;
                continue;
            }
            cout << "value of a: " << a << endl;
            a = a + 1;
        }
        while( a < 20 );
    }

    {
        // Local variable declaration:
        int a = 10;

        // do loop execution
        LOOP: do {
            if( a == 15) {
                // skip the iteration.
                a = a + 1;
                goto LOOP;
            }
            cout << "value of a: " << a << endl;
            a = a + 1;
        } while( a < 20 );
    }

    for( ; ; ) {
        printf("This loop will run forever.\n");
    }

    return 0;
}
