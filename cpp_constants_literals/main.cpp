#include <iostream>
using namespace std;

#define LENGTH 10
#define WIDTH  5
#define NEWLINE '\n'

int main() {
    cout << "Hello\tWorld\n\n";

    int area;

    area = LENGTH * WIDTH;
    cout << area;
    cout << NEWLINE;

    const int  _LENGTH = 10;
    const int  _WIDTH  = 5;
    const char _NEWLINE = '\n';

    area = _LENGTH * _WIDTH;
    cout << area;
    cout << _NEWLINE;

    return 0;
}