#include <iostream>
using namespace std;

/* This is a comment */

/* C++ comments can also
   * span multiple lines
*/

int main() {
    cout << "Hello World"; // prints Hello World

    return 0;
}