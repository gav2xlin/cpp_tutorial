#include <iostream>
using namespace std;

// Global variable declaration:
int g;

// Global variable declaration:
int _g = 20;

int main () {
    {
        // Local variable declaration:
        int a, b;
        int c;

        // actual initialization
        a = 10;
        b = 20;
        c = a + b;

        cout << c;
    }

    {
        // Local variable declaration:
        int a, b;

        // actual initialization
        a = 10;
        b = 20;
        g = a + b;

        cout << g;
    }

    {
        // Local variable declaration:
        int _g = 10;

        cout << _g;
    }

    return 0;
}