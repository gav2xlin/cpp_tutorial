#include <iostream>
#include <ctime>

using namespace std;

using namespace std;
const int MAX = 3;

void getSeconds(unsigned long *par);

// function declaration:
double getAverage(int *arr, int size);

// function to generate and retrun random numbers.
int * getRandom( ) {
    static int  r[10];

    // set the seed
    srand( (unsigned)time( NULL ) );

    for (int i = 0; i < 10; ++i) {
        r[i] = rand();
        cout << r[i] << endl;
    }

    return r;
}

int main () {
    {
        int var1;
        char var2[10];

        cout << "Address of var1 variable: ";
        cout << &var1 << endl;

        cout << "Address of var2 variable: ";
        cout << &var2 << endl;
    }

    {
        int  var = 20;   // actual variable declaration.
        int  *ip;        // pointer variable

        ip = &var;       // store address of var in pointer variable

        cout << "Value of var variable: ";
        cout << var << endl;

        // print the address stored in ip pointer variable
        cout << "Address stored in ip variable: ";
        cout << ip << endl;

        // access the value at the address available in pointer
        cout << "Value of *ip variable: ";
        cout << *ip << endl;
    }

    {
        int  *ptr = NULL;
        cout << "The value of ptr is " << ptr ;
    }

    {
        int  var[MAX] = {10, 100, 200};
        int  *ptr;

        // let us have array address in pointer.
        ptr = var;

        for (int i = 0; i < MAX; i++) {
            cout << "Address of var[" << i << "] = ";
            cout << ptr << endl;

            cout << "Value of var[" << i << "] = ";
            cout << *ptr << endl;

            // point to the next location
            ptr++;
        }
    }

    {
        int  var[MAX] = {10, 100, 200};
        int  *ptr;

        // let us have address of the last element in pointer.
        ptr = &var[MAX-1];

        for (int i = MAX; i > 0; i--) {
            cout << "Address of var[" << i << "] = ";
            cout << ptr << endl;

            cout << "Value of var[" << i << "] = ";
            cout << *ptr << endl;

            // point to the previous location
            ptr--;
        }
    }

    {
        int  var[MAX] = {10, 100, 200};
        int  *ptr;

        // let us have address of the first element in pointer.
        ptr = var;
        int i = 0;

        while ( ptr <= &var[MAX - 1] ) {
            cout << "Address of var[" << i << "] = ";
            cout << ptr << endl;

            cout << "Value of var[" << i << "] = ";
            cout << *ptr << endl;

            // point to the previous location
            ptr++;
            i++;
        }
    }

    {
        int  var[MAX] = {10, 100, 200};
        int  *ptr;

        // let us have array address in pointer.
        ptr = var;

        for (int i = 0; i < MAX; i++) {
            cout << "Address of var[" << i << "] = ";
            cout << ptr << endl;

            cout << "Value of var[" << i << "] = ";
            cout << *ptr << endl;

            // point to the next location
            ptr++;
        }
    }

    {
        int  var[MAX] = {10, 100, 200};

        for (int i = 0; i < MAX; i++) {
            *var = i;    // This is a correct syntax
            // var++;       // This is incorrect.
        }
    }

    {
        int  var[MAX] = {10, 100, 200};

        for (int i = 0; i < MAX; i++) {

            cout << "Value of var[" << i << "] = ";
            cout << var[i] << endl;
        }
    }

    {
        int  var[MAX] = {10, 100, 200};
        int *ptr[MAX];

        for (int i = 0; i < MAX; i++) {
            ptr[i] = &var[i]; // assign the address of integer.
        }

        for (int i = 0; i < MAX; i++) {
            cout << "Value of var[" << i << "] = ";
            cout << *ptr[i] << endl;
        }
    }

    {
        const int MAX = 4;
        const char *names[MAX] = { "Zara Ali", "Hina Ali", "Nuha Ali", "Sara Ali" };

        for (int i = 0; i < MAX; i++) {
            cout << "Value of names[" << i << "] = ";
            cout << (names + i) << endl;
        }
    }

    {
        int  var;
        int  *ptr;
        int  **pptr;

        var = 3000;

        // take the address of var
        ptr = &var;

        // take the address of ptr using address of operator &
        pptr = &ptr;

        // take the value using pptr
        cout << "Value of var :" << var << endl;
        cout << "Value available at *ptr :" << *ptr << endl;
        cout << "Value available at **pptr :" << **pptr << endl;
    }

    {
        unsigned long sec;
        getSeconds( &sec );

        // print the actual value
        cout << "Number of seconds :" << sec << endl;
    }

    {
        // an int array with 5 elements.
        int balance[5] = {1000, 2, 3, 17, 50};
        double avg;

        // pass pointer to the array as an argument.
        avg = getAverage( balance, 5 ) ;

        // output the returned value
        cout << "Average value is: " << avg << endl;
    }

    {
        // a pointer to an int.
        int *p;

        p = getRandom();
        for ( int i = 0; i < 10; i++ ) {
            cout << "*(p + " << i << ") : ";
            cout << *(p + i) << endl;
        }
    }

    return 0;
}

void getSeconds(unsigned long *par) {
    // get the current number of seconds
    *par = time( NULL );
}

double getAverage(int *arr, int size) {
    int i, sum = 0;
    double avg;

    for (i = 0; i < size; ++i) {
        sum += arr[i];
    }
    avg = double(sum) / size;

    return avg;
}
