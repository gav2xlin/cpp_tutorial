#include <iostream>

using namespace std;

int main() {
    {
        int a = 21;
        int b = 10;
        int c ;

        c = a + b;
        cout << "Line 1 - Value of c is :" << c << endl ;

        c = a - b;
        cout << "Line 2 - Value of c is  :" << c << endl
                ;
        c = a * b;
        cout << "Line 3 - Value of c is :" << c << endl ;

        c = a / b;
        cout << "Line 4 - Value of c is  :" << c << endl ;

        c = a % b;
        cout << "Line 5 - Value of c is  :" << c << endl ;

        c = a++;
        cout << "Line 6 - Value of c is :" << c << endl ;

        c = a--;
        cout << "Line 7 - Value of c is  :" << c << endl ;
    }

    {
        int a = 21;
        int b = 10;
        int c ;

        if( a == b ) {
            cout << "Line 1 - a is equal to b" << endl ;
        } else {
            cout << "Line 1 - a is not equal to b" << endl ;
        }

        if( a < b ) {
            cout << "Line 2 - a is less than b" << endl ;
        } else {
            cout << "Line 2 - a is not less than b" << endl ;
        }

        if( a > b ) {
            cout << "Line 3 - a is greater than b" << endl ;
        } else {
            cout << "Line 3 - a is not greater than b" << endl ;
        }

        /* Let's change the values of a and b */
        a = 5;
        b = 20;
        if( a <= b ) {
            cout << "Line 4 - a is either less than \\ or equal to  b" << endl ;
        }

        if( b >= a ) {
            cout << "Line 5 - b is either greater than \\ or equal to b" << endl ;
        }
    }

    {
        int a = 5;
        int b = 20;
        int c ;

        if(a && b) {
            cout << "Line 1 - Condition is true"<< endl ;
        }

        if(a || b) {
            cout << "Line 2 - Condition is true"<< endl ;
        }

        /* Let's change the values of  a and b */
        a = 0;
        b = 10;

        if(a && b) {
            cout << "Line 3 - Condition is true"<< endl ;
        } else {
            cout << "Line 4 - Condition is not true"<< endl ;
        }

        if(!(a && b)) {
            cout << "Line 5 - Condition is true"<< endl ;
        }
    }

    {
        unsigned int a = 60;	  // 60 = 0011 1100
        unsigned int b = 13;	  // 13 = 0000 1101
        int c = 0;

        c = a & b;             // 12 = 0000 1100
        cout << "Line 1 - Value of c is : " << c << endl ;

        c = a | b;             // 61 = 0011 1101
        cout << "Line 2 - Value of c is: " << c << endl ;

        c = a ^ b;             // 49 = 0011 0001
        cout << "Line 3 - Value of c is: " << c << endl ;

        c = ~a;                // -61 = 1100 0011
        cout << "Line 4 - Value of c is: " << c << endl ;

        c = a << 2;            // 240 = 1111 0000
        cout << "Line 5 - Value of c is: " << c << endl ;

        c = a >> 2;            // 15 = 0000 1111
        cout << "Line 6 - Value of c is: " << c << endl ;
    }

    {
        int a = 21;
        int c ;

        c =  a;
        cout << "Line 1 - =  Operator, Value of c = : " <<c<< endl ;

        c +=  a;
        cout << "Line 2 - += Operator, Value of c = : " <<c<< endl ;

        c -=  a;
        cout << "Line 3 - -= Operator, Value of c = : " <<c<< endl ;

        c *=  a;
        cout << "Line 4 - *= Operator, Value of c = : " <<c<< endl ;

        c /=  a;
        cout << "Line 5 - /= Operator, Value of c = : " <<c<< endl ;

        c  = 200;
        c %=  a;
        cout << "Line 6 - %= Operator, Value of c = : " <<c<< endl ;

        c <<=  2;
        cout << "Line 7 - <<= Operator, Value of c = : " <<c<< endl ;

        c >>=  2;
        cout << "Line 8 - >>= Operator, Value of c = : " <<c<< endl ;

        c &=  2;
        cout << "Line 9 - &= Operator, Value of c = : " <<c<< endl ;

        c ^=  2;
        cout << "Line 10 - ^= Operator, Value of c = : " <<c<< endl ;

        c |=  2;
        cout << "Line 11 - |= Operator, Value of c = : " <<c<< endl ;
    }

    {
        cout << "Size of char : " << sizeof(char) << endl;
        cout << "Size of int : " << sizeof(int) << endl;
        cout << "Size of short int : " << sizeof(short int) << endl;
        cout << "Size of long int : " << sizeof(long int) << endl;
        cout << "Size of float : " << sizeof(float) << endl;
        cout << "Size of double : " << sizeof(double) << endl;
        cout << "Size of wchar_t : " << sizeof(wchar_t) << endl;
    }

    {
        // Local variable declaration:
        int x, y = 10;

        x = (y < 10) ? 30 : 40;
        cout << "value of x: " << x << endl;
    }

    {
        int i, j;

        j = 10;
        i = (j++, j+100, 999+j);

        cout << i;
    }

    {
        struct Employee {
            char first_name[16];
            int  age;
        } emp;

        Employee* p_emp = &emp;

        strcpy(emp.first_name, "zara");
        strcpy(p_emp->first_name, "zara");
    }

    {
        double a = 21.09399;
        float b = 10.20;
        int c ;

        c = (int) a;
        cout << "Line 1 - Value of (int)a is :" << c << endl ;

        c = (int) b;
        cout << "Line 2 - Value of (int)b is  :" << c << endl ;
    }

    {
        int  var;
        int  *ptr;
        int  val;

        var = 3000;

        // take the address of var
        ptr = &var;

        // take the value available at ptr
        val = *ptr;
        cout << "Value of var :" << var << endl;
        cout << "Value of ptr :" << ptr << endl;
        cout << "Value of val :" << val << endl;
    }

    {
        int  var;
        int  *ptr;
        int  val;

        var = 3000;

        // take the address of var
        ptr = &var;

        // take the value available at ptr
        val = *ptr;
        cout << "Value of var :" << var << endl;
        cout << "Value of ptr :" << ptr << endl;
        cout << "Value of val :" << val << endl;
    }

    {
        int a = 20;
        int b = 10;
        int c = 15;
        int d = 5;
        int e;

        e = (a + b) * c / d;      // ( 30 * 15 ) / 5
        cout << "Value of (a + b) * c / d is :" << e << endl ;

        e = ((a + b) * c) / d;    // (30 * 15 ) / 5
        cout << "Value of ((a + b) * c) / d is  :" << e << endl ;

        e = (a + b) * (c / d);   // (30) * (15/5)
        cout << "Value of (a + b) * (c / d) is  :" << e << endl ;

        e = a + (b * c) / d;     //  20 + (150/5)
        cout << "Value of a + (b * c) / d is  :" << e << endl ;
    }

    return 0;
}
