#include <iostream>
using namespace std;

// first name space
namespace first_space {
    void func() {
        cout << "Inside first_space" << endl;
    }
}

// second name space
namespace second_space {
    void func() {
        cout << "Inside second_space" << endl;
    }
}

using namespace first_space;

// first name space
namespace _first_space {
    void _func() {
        cout << "Inside first_space" << endl;
    }

    // second name space
    namespace _second_space {
        void _func() {
            cout << "Inside second_space" << endl;
        }
    }
}

using namespace _first_space::_second_space;

int main () {
    // Calls function from first name space.
    first_space::func();

    // Calls function from second name space.
    second_space::func();

    // This calls function from first name space.
    func();

    using std::cout;
    cout << "std::endl is used with std!" << std::endl;

    // This calls function from second name space.
    _func();

    return 0;
}