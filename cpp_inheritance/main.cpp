#include <iostream>

using namespace std;

// Base class
class Shape {
public:
    void setWidth(int w) {
        width = w;
    }
    void setHeight(int h) {
        height = h;
    }

protected:
    int width;
    int height;
};

// Derived class
class Rectangle: public Shape {
public:
    int getArea() {
        return (width * height);
    }
};

// Base class Shape
class Shape2 {
public:
    void setWidth(int w) {
        width = w;
    }
    void setHeight(int h) {
        height = h;
    }

protected:
    int width;
    int height;
};

// Base class PaintCost
class PaintCost2 {
public:
    int getCost(int area) {
        return area * 70;
    }
};

// Derived class
class Rectangle2: public Shape2, public PaintCost2 {
public:
    int getArea() {
        return (width * height);
    }
};

int main(void) {
    {
        Rectangle Rect;

        Rect.setWidth(5);
        Rect.setHeight(7);

        // Print the area of the object.
        cout << "Total area: " << Rect.getArea() << endl;
    }

    {
        Rectangle2 Rect;
        int area;

        Rect.setWidth(5);
        Rect.setHeight(7);

        area = Rect.getArea();

        // Print the area of the object.
        cout << "Total area: " << Rect.getArea() << endl;

        // Print the total cost of painting
        cout << "Total paint cost: $" << Rect.getCost(area) << endl;
    }

    return 0;
}