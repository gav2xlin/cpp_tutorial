#include <iostream>

// Function declaration
void func(void);

static int count = 10; /* Global variable */

int _count ;
extern void write_extern();

int main() {
    {
        int mount;
        auto int month;
    }

    {
        register int  miles;
    }

    {
        while(count--) {
            func();
        }
    }

    {
        _count = 5;
        write_extern();
    }

    return 0;
}

// Function definition
void func( void ) {
    static int i = 5; // local static variable
    i++;
    std::cout << "i is " << i ;
    std::cout << " and count is " << count << std::endl;
}
