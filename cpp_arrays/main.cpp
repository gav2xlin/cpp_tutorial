#include <iostream>
#include <iomanip>
#include <ctime>

using namespace std;
// using std::setw;

// function declaration:
double getAverage(int arr[], int size);

// function to generate and retrun random numbers.
int * getRandom( ) {

    static int  r[10];

    // set the seed
    srand( (unsigned)time( NULL ) );

    for (int i = 0; i < 10; ++i) {
        r[i] = rand();
        cout << r[i] << endl;
    }

    return r;
}

int main() {
    {
        int n[ 10 ]; // n is an array of 10 integers

        // initialize elements of array n to 0
        for ( int i = 0; i < 10; i++ ) {
            n[ i ] = i + 100; // set element at location i to i + 100
        }
        cout << "Element" << setw( 13 ) << "Value" << endl;

        // output each array element's value
        for ( int j = 0; j < 10; j++ ) {
            cout << setw( 7 )<< j << setw( 13 ) << n[ j ] << endl;
        }
    }

    {
        // an array with 5 rows and 2 columns.
        int a[5][2] = { {0,0}, {1,2}, {2,4}, {3,6},{4,8}};

        // output each array element's value
        for ( int i = 0; i < 5; i++ )
            for ( int j = 0; j < 2; j++ ) {

                cout << "a[" << i << "][" << j << "]: ";
                cout << a[i][j]<< endl;
            }
    }

    {
        // an array with 5 elements.
        double balance[5] = {1000.0, 2.0, 3.4, 17.0, 50.0};
        double *p;

        p = balance;

        // output each array element's value
        cout << "Array values using pointer " << endl;

        for ( int i = 0; i < 5; i++ ) {
            cout << "*(p + " << i << ") : ";
            cout << *(p + i) << endl;
        }
        cout << "Array values using balance as address " << endl;

        for ( int i = 0; i < 5; i++ ) {
            cout << "*(balance + " << i << ") : ";
            cout << *(balance + i) << endl;
        }
    }

    {
        // an int array with 5 elements.
        int balance[5] = {1000, 2, 3, 17, 50};
        double avg;

        // pass pointer to the array as an argument.
        avg = getAverage( balance, 5 ) ;

        // output the returned value
        cout << "Average value is: " << avg << endl;
    }

    {
        // a pointer to an int.
        int *p;

        p = getRandom();

        for ( int i = 0; i < 10; i++ ) {
            cout << "*(p + " << i << ") : ";
            cout << *(p + i) << endl;
        }
    }

    return 0;
}

double getAverage(int arr[], int size) {
    int i, sum = 0;
    double avg;

    for (i = 0; i < size; ++i) {
        sum += arr[i];
    }
    avg = double(sum) / size;

    return avg;
}