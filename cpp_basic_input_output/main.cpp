#include <iostream>

using namespace std;

int main() {
    {
        char str[] = "Hello C++";

        cout << "Value of str is : " << str << endl;
    }

    {
        char name[50];

        cout << "Please enter your name: ";
        cin >> name;
        cout << "Your name is: " << name << endl;
    }

    {
        char str[] = "Unable to read....";

        cerr << "Error message : " << str << endl;
    }

    {
        char str[] = "Unable to read....";

        clog << "Error message : " << str << endl;
    }
}